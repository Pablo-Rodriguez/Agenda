
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#define FECHAS

#ifndef UTIL_LIB
	#include "util.h"
#endif

//Libreria que sirve para el manejo de fechas
int esBisiesto(int anho);
int esUnaFecha(char *fecha);
void analizarFecha(char *fecha, int *destino);
int mes31 (int mes);
int mes30 (int mes);
int esFechaFutura(char *fecha);
int diasQueFaltan(char *fechaTarea);
struct tm fechaActual();