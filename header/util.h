
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define UTIL_LIB

//Utilidades recurrentes a lo largo del programa
void memcpypos(char *dest, char *org, int tam, int pos);
int strnum (char *str, int n);
int estaEnElArray(int n, int *arr, int tam);
void quitarSalto(char *str, int tam);
void limpiarBuffer(char *str, int max);
void borrarBuffer();