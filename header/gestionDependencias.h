
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#define GESTION_DEPENDENCIAS

#ifndef UTIL_LIB
	#include "util.h"
#endif

#ifndef SET_LIB
	#include "set.h"
#endif

#ifndef TAREAS_BD
	#include "tareasBD.h"
#endif

//Libreria para el analizado y uso de dependencias en las tareas
//utilidades
int digitosSeparados (char *str, char separador, int n);

//Funciones principales de la libreria
int numeroDeDependencias (char *dep, char separador);
int sonDependencias (char *dep);
int *analizarDependencias (char *dep, int *tam, char separador);
int dependenciaEnLista (Tarea *lista, int dependencia);
int existenDependencias (Tarea *lista, char *dep);
void buscarDependencias(Tarea *lista,int ids);
void DependenciasIndir(Tarea *lista,int idTar, Set *s);