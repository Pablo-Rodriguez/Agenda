
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#define PANTALLA

#ifndef GESTION_TAREAS
	#include "gestionTareas.h"
#endif

#define MAX_CARACTERES_LINEA 80
#define CREAR     '1'
#define VER       '2'
#define LISTAR    '3'
#define ELIMINAR  '4'
#define TERMINAR  '5'
#define SALIR     '0'

//Libreria con funciones de presentacion por pantalla y recogida de datos
void lineas (int linea, int cantidad, char c);
int confirmar (char *frase);
void titulo (char *frase, int tam, char c);
void mostrarMenu ();
int seleccionarOpcion (Tarea **lista, char *usuario, int mod);