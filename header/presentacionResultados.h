
#include <stdio.h>

#define PRESENTACION_RESULTADOS

#ifndef TAREAS_BD
	#include "tareasBD.h"
#endif

#ifndef PANTALLA
	#include "pantalla.h"
#endif

//Libreria de salida de datos tanto por pantalla como a fichero
int presentaResultados (Tarea *lista, char eleccion, char *usuario);
void verPorPantalla(Tarea *lista, char eleccion, char *usuario);
int validarNombreFichero (char *s);
void guardaEnFichero(Tarea *lista, char eleccion, char *usuario);