
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define TAREAS_BD

#ifndef UTIL_LIB
	#include "util.h"
#endif

#define BASE_DE_DATOS "tareas.txt"

//Libreria del manejador de la base de datos para la coleccion: Tareas
struct tarea {
	char tipo[15];
	int id;
	char descripcion[50];
	char propietario[20];
	int prioridad;
	char estado[15];
	char plazo[20];
	char dependencias[50];
	struct tarea *siguiente;
};

typedef struct tarea Tarea;

//Funciones de carga
Tarea *cargarTareas();
int tamanhoLista(Tarea *lista);
Tarea *cargarTareaPorId (int id);
void liberarLista(Tarea *lista);

//Funciones de creacion
int tareaExistente(Tarea *lista, char *descripcion);
int buscarIdDisponible();
void escribirTareasEnBD(Tarea *t);
Tarea *anhadirALista(Tarea *t, Tarea *lista);

//Funciones de lista de tareas
int presentaResultados (Tarea *lista, char eleccion, char *usuario);
void verPorPantalla(Tarea *lista, char eleccion, char *usuario);
void guardaEnFichero(Tarea *lista, char eleccion, char *usuario);