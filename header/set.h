
#include <stdio.h>
#include <stdlib.h>

#define SET_LIB

#ifndef UTIL_LIB
	#include "util.h"
#endif

//Libreria de la "clase" Set que es un "array" de elementos unicos
//Si se intenta anhadir algo al set (por medio de la funcion pushSet) que ya
//este en el, no se vuelve a anhadir
struct set {
    int *arr;
    int tam;
};

typedef struct set Set;

void initSet(Set *s);
void pushSet(Set *s, int elem);
void printSet(Set *s);
void liberarSet(Set *s);