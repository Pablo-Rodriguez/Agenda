
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define GESTION_TAREAS

#ifndef TAREAS_BD
	#include "tareasBD.h"
#endif

#ifndef SET_LIB
	#include "set.h"
#endif

#ifndef PANTALLA
	#include "pantalla.h"
#endif

#ifndef FECHAS
	#include "fechas.h"
#endif

#ifndef GESTION_DEPENDENCIAS
	#include "gestionDependencias.h"
#endif

#ifndef PRESENTACION_RESULTADOS
	#include "presentacionResultados.h"
#endif

//Libreria con la funcionalidad principal del programa
Tarea *crearTarea(Tarea *lista, char *usuario);
int verLista(Tarea *lista, char *usuario);
void verTarea(Tarea *lista,char *usuario);
Tarea *eliminarTarea(Tarea *lista, char *usuario);
void terminarTarea(Tarea *lista, char *usuario);
void salir(Tarea *lista, int mod);