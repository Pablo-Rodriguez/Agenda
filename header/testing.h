
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define TESTING

//Libreria basica creada para testing
//Sirve para logger durante la ejecucion del programa
//a un fichero con el proposito de revisarlo despues
//para comprobar el resultado de dicha ejecucion

int configTest (char *fileName, char *mode);
void printConfig();
void startTesting();
void stopTesting();
int logger (char *text);