install: pro
	mkdir /usr/local/Agenda
	cp ./Agenda /usr/local/Agenda
	touch /usr/local/Agenda/tareas.txt
	chmod a+rw -R /usr/local/Agenda
	ln -s /usr/local/Agenda/Agenda /usr/bin
uninstall:
	rm /usr/local/Agenda/Agenda
	rm /usr/bin/Agenda
	rm -R /usr/local/Agenda
pro: build build/util.o build/set.o build/tareasBD.o build/gestionTareas.o build/pantalla.o build/fechas.o build/gestionDependencias.o build/presentacionResultados.o
	gcc -Wall -o Agenda main.c build/util.o build/set.o build/tareasBD.o build/pantalla.o build/gestionTareas.o build/fechas.o build/gestionDependencias.o build/presentacionResultados.o
dev: build build/testing.o build/util.o build/set.o build/tareasBD.o build/gestionTareas.o build/pantalla.o build/fechas.o build/gestionDependencias.o build/presentacionResultados.o
	gcc -Wall -o Agenda main.c build/testing.o build/util.o build/set.o build/tareasBD.o build/pantalla.o build/gestionTareas.o build/fechas.o build/gestionDependencias.o build/presentacionResultados.o
build:
	mkdir build
build/testing.o: lib/testing.c
	gcc -Wall -g -c lib/testing.c -o build/testing.o
build/util.o: lib/util.c
	gcc -Wall -g -c lib/util.c -o build/util.o
build/set.o: lib/set.c
	gcc -Wall -g -c lib/set.c -o build/set.o
build/tareasBD.o: lib/tareasBD.c
	gcc -Wall -g -c lib/tareasBD.c -o build/tareasBD.o
build/gestionTareas.o: lib/gestionTareas.c
	gcc -Wall -g -c lib/gestionTareas.c -o build/gestionTareas.o
build/pantalla.o: lib/pantalla.c
	gcc -Wall -g -c lib/pantalla.c -o build/pantalla.o
build/fechas.o: lib/fechas.c
	gcc -Wall -g -c lib/fechas.c -o build/fechas.o
build/gestionDependencias.o: lib/gestionDependencias.c
	gcc -Wall -g -c lib/gestionDependencias.c -o build/gestionDependencias.o
build/presentacionResultados.o: lib/presentacionResultados.c
	gcc -Wall -g -c lib/presentacionResultados.c -o build/presentacionResultados.o