
#include "../header/presentacionResultados.h"

//Presentacion de los resultados de ver lista de tareas
//Recibe la lista de tareas completa, la eleccion del submenu 1 (pendientes, terminadas o todas)
//y el string con el nombre de usuario que ha entrado
//devuelve 1 si se selecciona ver por pantalla. 0 en otro caso
int presentaResultados (Tarea *lista, char eleccion, char *usuario) {
	char option2; //Opciones del submenu 2
	int espera = 0; //devuelve 1 si se muestra por pantalla
	//Submenu 2 (como visualizar el listado creado segun el criterio submenu 1)
	while(1){
		printf("\t1) Por pantalla\n\n");
		printf("\t2) A fichero\n\n");
		printf("\t0) Cancelar\n\n");
		printf("\t¿Presentacion del resultado?:");
		scanf(" %c",&option2);

		switch(option2){

			case '1':
				//Presentar resultados por pantalla
				system("clear");
				espera = 1;
				verPorPantalla(lista, eleccion, usuario);
				return espera;

			case '2':
				guardaEnFichero(lista, eleccion, usuario);
				return espera;

			case '0':
				borrarBuffer();
				system ("clear");
				return espera;

			default:
				system("clear");
				borrarBuffer();
				printf("\n\t\033[31mOpcion invalida. Seleccione 1,2 o 0 y pulse enter.\x1b[0m\n\n");
				break;
		}
	}
	return espera;
}
//Ver por pantalla segun el criterio del submenu 1
//Recibe la lista completa, la eleccion del submenu 1 y el string con el usuario del programa
void verPorPantalla(Tarea *lista, char eleccion, char *usuario) {
	//Recorre la lista
	while(lista != NULL){
		//Si la eleccion del submenu 1 es pendiente, muestra por pantalla las tareas pendientes
		//del usuario que ha entrado
		if(eleccion=='1'){									
			if(strcmp(lista->estado,"pendiente")==0 && strcmp(lista->propietario,usuario)==0){
				lineas(1,80,'*');
				printf("\t\t\t");
				printf("\tTAREA %d\n", lista->id);
				lineas(1,80,'*');
				printf("\tTipo: %s\n", lista->tipo);
				printf("\tDescripcion: %s\n", lista->descripcion);
				printf("\tPropietario: %s\n", lista->propietario);
				printf("\tPrioridad: %d\n", lista->prioridad);
				printf("\tEstado: %s\n", lista->estado);
				printf("\tPlazo: %s\n", lista->plazo);
				printf("\tDependencias: %s\n", lista->dependencias);
				printf("\n");
			}
		}
		if(eleccion=='2'){
			if(strcmp(lista->estado,"terminada")==0 && strcmp(lista->propietario,usuario)==0){
				lineas(1,80,'*');
				printf("\t\t\t");
				printf("\tTAREA %d\n", lista->id);
				lineas(1,80,'*');
				printf("\tTipo: %s\n", lista->tipo);
				printf("\tDescripcion: %s\n", lista->descripcion);
				printf("\tPropietario: %s\n", lista->propietario);
				printf("\tPrioridad: %d\n", lista->prioridad);
				printf("\tEstado: %s\n", lista->estado);
				printf("\tPlazo: %s\n", lista->plazo);
				printf("\tDependencias: %s\n", lista->dependencias);
				printf("\n");
			}
		}
		if(eleccion=='3'){
			if(strcmp(lista->propietario,usuario)==0){
				lineas(1,80,'*');
				printf("\t\t\t");
				printf("\tTAREA %d\n", lista->id);
				lineas(1,80,'*');
				printf("\tTipo: %s\n", lista->tipo);
				printf("\tDescripcion: %s\n", lista->descripcion);
				printf("\tPropietario: %s\n", lista->propietario);
				printf("\tPrioridad: %d\n", lista->prioridad);
				printf("\tEstado: %s\n", lista->estado);
				printf("\tPlazo: %s\n", lista->plazo);
				printf("\tDependencias: %s\n", lista->dependencias);
				printf("\n");
			}
		}
		lista = lista->siguiente;
	}
	return;
}

//Devuelve un 1 si el nombre de fichero es correcto. 0 en caso contrario
int validarNombreFichero (char *s) {
	int i = strlen(s);
	if(s[i-1] == 't' && s[i-2] == 'x' && s[i-3] == 't' && s[i-4] == '.'){
		return 1;
	}else{
		return 0;
	}
}

//Guardar en un fichero segun el criterio del submenu 1
//Recibe la lista completa (puntero al inicio de la lista), la eleccion del submenu 1 y el usuario 
void guardaEnFichero(Tarea *lista, char eleccion, char *usuario) { 
	char nomFich[40];
	char opcionElegida[15];

	scanf("%*c");
	//Introducir el nombre del fichero en el que se va a guardar la lista, segun el criterio del submenu 1
	while(1) {
		printf("\n\tIndicar el identificador del fichero (nombre con extension .txt): ");
		fgets(nomFich, 40, stdin);
		quitarSalto(nomFich, strlen(nomFich));
		limpiarBuffer(nomFich, 38);

		puts(nomFich);

		//Compruebo que se ha introduccido bien el nombre del fichero 
		if(!validarNombreFichero(nomFich)) {
			printf("\n\tERROR: \033[31mEl fichero debe tener extension .txt y no puede superar los 40 caracteres.\x1b[0m\n");
		}else{
			break;
		}
	}


	//Declaro un puntero a archivo para escribir en la bd indicada por el usuario
	FILE *bd;
	//Variable auxiliar para recorrer la lista
	Tarea *aux;
	aux = lista;
	//Abro el archivo en modo append para escribir al final
	bd = fopen(nomFich, "w");

	if(bd == NULL){
		printf("\tERROR: \033[31mNo se ha podido abrir el fichero: %s\n\x1b[0m", nomFich);
		scanf("%*c");
		printf("\tPulsar ENTER para salir al programa.\n");
		getchar();
		exit(1);
	}

	switch(eleccion) {
		case '1':
			//pendientes
			strcpy(opcionElegida,"pendiente");
			break;

		case '2':
			//terminadas
			strcpy(opcionElegida,"terminada");
			break;

		case '3':
			//todas
			//Escribo los datos en el fichero en el caso de todas las tareas del usuario
			while(aux != NULL){
				if(strcmp(aux->propietario,usuario)==0) {
					fprintf(bd, "%s\n", aux->tipo);
					fprintf(bd, "%d\n", aux->id);
					fprintf(bd, "%s\n", aux->descripcion);
					fprintf(bd, "%s\n", aux->propietario);
					fprintf(bd, "%d\n", aux->prioridad);
					fprintf(bd, "%s\n", aux->estado);
					fprintf(bd, "%s\n", aux->plazo);
					fprintf(bd, "%s\n", aux->dependencias);
					if(aux->siguiente == NULL){
						fprintf(bd, "*");
					}else{
						fprintf(bd, "*\n");
					}
					aux = aux->siguiente;
				}else{
					aux = aux->siguiente;
				}
			}
			fclose(bd);
			return;

		default:
			return;
	}
	//Escribo los datos en el nuevo fichero creado (si son pendientes o terminadas)
	while(aux != NULL){
		if(strcmp(aux->propietario,usuario)==0 && strcmp(aux->estado,opcionElegida)==0){
			fprintf(bd, "%s\n", aux->tipo);
			fprintf(bd, "%d\n", aux->id);
			fprintf(bd, "%s\n", aux->descripcion);
			fprintf(bd, "%s\n", aux->propietario);
			fprintf(bd, "%d\n", aux->prioridad);
			fprintf(bd, "%s\n", aux->estado);
			fprintf(bd, "%s\n", aux->plazo);
			fprintf(bd, "%s\n", aux->dependencias);
			if(aux->siguiente == NULL){
				fprintf(bd, "*");
			}else{
				fprintf(bd, "*\n");
			}
			aux = aux->siguiente;
		}else{
			aux = aux->siguiente;
		}
	}
	fclose(bd);
	return;
}