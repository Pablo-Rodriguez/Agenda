
#include "../header/pantalla.h"

//Archivo pantalla.c que incluye funciones de entrada y salida

//Funcion que imprime por pantalla lineas de caracteres
//linea: lineas cantidad: caracteres por linea c: caracter
void lineas (int linea, int cantidad, char c) {
  int i = 0, j = 0;
  if(cantidad > MAX_CARACTERES_LINEA){
  	cantidad = MAX_CARACTERES_LINEA;
  }
  for(i = 0; i < linea; i++){
    for(j = 0; j < cantidad; j++){
      printf("%c", c);
    }
    printf("\n");
  }
  return;
}

//Funcion que pide confirmacion para realizar una accion
//muestra por pantalla la frase pasada por parametros
int confirmar (char *frase) {
  char c = '0';
  puts(frase); // Imprime por pantalla la frase que recibe por parametros de la funcion
  scanf(" %c", &c);
  if(tolower(c) == 's'){
    return 1;
  }else if(tolower(c) == 'n'){
    return 0;
  }else{
    return -1;
  }
}

//Funcion parecida a lineas pero que solo imprime una 
//con una frase centrada rodeada por los caracteres
void titulo (char *frase, int tam, char c) {
  int cantidad_c, cantidad1, cantidad2, i;
  if(tam > MAX_CARACTERES_LINEA){
    tam = MAX_CARACTERES_LINEA;
  }

  cantidad_c = tam - strlen(frase);
  if(cantidad_c % 2 == 0){
    cantidad1 = cantidad2 = cantidad_c/2;
  }else{
    cantidad1 = (cantidad_c/2) + 1;
    cantidad2 = cantidad_c / 2;
  }

  for(i = 0; i < cantidad1; i++){
    printf("%c", c);
  }

  printf("%s", frase);

  for(i = 0; i < cantidad2; i++){
    printf("%c", c);
  }
  printf("\n");

  return;
}

//Muestra por pantalla el menu principal del programa
void mostrarMenu () {
  printf("\tAgenda:\n");
  printf("\t1) Crear tarea.\n");
  printf("\t2) Ver tarea.\n");
  printf("\t3) Ver lista de tareas.\n");
  printf("\t4) Eliminar tarea.\n");
  printf("\t5) Terminar tarea.\n");
  printf("\t0) Salir\n");
  printf("\t¿Siguiente operacion?\n");
}

//Funcion que contiene la logica del menu principal del programa
int seleccionarOpcion(Tarea **lista, char *usuario, int mod) {
  char c;
  printf("\t");
  scanf("%c", &c);
  switch(tolower(c)) {
    case CREAR:
      //Devuelve el puntero modificado debido a la creacion de
      //de una tarea nueva
      *lista = crearTarea(*lista, usuario);
      borrarBuffer();
      system("clear");
      return 1;
      break;
    case VER:
      verTarea(*lista, usuario);
      borrarBuffer();
      system("clear");
      return 0;
      break;
    case LISTAR:
      verLista(*lista, usuario);
      system("clear");
      return 0;
      break;
    case SALIR:    
      salir(*lista, mod);
      borrarBuffer();
      return 0;
      break;
    case ELIMINAR:
      *lista = eliminarTarea(*lista, usuario);
      scanf("%*c");
      printf("\tPulse enter para continuar...");
      borrarBuffer();
      system("clear");
      return 1;
      break;
    case TERMINAR:
      terminarTarea(*lista, usuario);
      scanf("%*c");
      printf("\tPulse enter para continuar...");
      borrarBuffer();
      system("clear");
      return 1;
      break;
    default:
      system("clear");
      printf("\033[31m\tELECCION INCORRECTA, PULSAR 1, 2, 3, 4, 5 o 0.\x1b[0m\n\n");
      borrarBuffer();
      return 0;
  }
  return 1;
}