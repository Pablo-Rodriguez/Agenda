
#include "../header/gestionDependencias.h"

//Funcion que devuelve 1 si el string solo tiene digitos y/o
//el separador y 0 en el caso contrario. Revisando los n primeros elementos
//los espacios no se tienen en cuena para devolver un 0
int digitosSeparados (char *str, char separador, int n) {
	int len = strlen(str), i = 0;
	if(n > len){
		n = len;
	}

	for(i = 0; i < n; i++){
		if(!isdigit(str[i]) && str[i] != separador && !isspace(str[i])){
			return 0;
		}
		if(isspace(str[i]) && i != (n-1)){
			str[i] = ',';
		}
	}

	return 1;
}

//Devuelve el numero de dependencias que hai en un string de dependencias
//formato: n1,n2,n3...,nn
int numeroDeDependencias (char *dep, char separador) {
	int i = 0, numeros = 0, cantidad = 0;
	for(i = 0; dep[i] != '\0'; i++){
		if(numeros == 0 && isdigit(dep[i])){
			numeros = 1;
		}
		if((dep[i] == separador || isspace(dep[i]) || dep[i+1] == '\0') && numeros != 0){
			cantidad++;
			numeros = 0;
		}
	}
	return cantidad;
}

//Funcion que retorna uno si el string pasado 
//corresponde con el modelo de introduccion de dependencias
// ej: 1,23,4,5 return 1. s,1,2,d,f return 0.
int sonDependencias (char *dep) {

	//Comprueba que no hai caracteres raros
	if(!digitosSeparados(dep, ',', strlen(dep)) || !strcmp(dep, "\n")){
		return 0;
	}

	return 1;
}

//Funcion que devuelve un array de enteros con
//los ids de las dependencias de un string de dependencias
int *analizarDependencias (char *dep, int *tam, char separador) {
	int n = numeroDeDependencias(dep, separador), i = 0, j = 0;
	*tam = n;
	int *dependencias = malloc(n*sizeof(int));
	int numeros = 0, lastPos = 0;
	char dato[10];
	for(i = 0; dep[i] != '\0'; i++){
		if(numeros == 0 && isdigit(dep[i])){
			numeros = 1;
			lastPos = i;
		}
		if((dep[i] == separador || isspace(dep[i])) && numeros != 0){
			memcpypos(dato, dep, i - lastPos, lastPos);
			*(dependencias + j) = atoi(dato);
			j++;
			numeros = 0;
		}
		if(numeros != 0 && dep[i+1] == '\0'){
			memcpypos(dato, dep, (i+1) - lastPos, lastPos);
			*(dependencias + j) = atoi(dato);
			j++;
			numeros = 0;
		}
	}
	return dependencias;
}

//Devuelve un 1 si existe la tarea con id: dependencia en la lista
//0 en caso contrario
int dependenciaEnLista (Tarea *lista, int dependencia) {
	Tarea *aux = lista;

	while(aux != NULL){
		if(aux->id == dependencia){
			return 1;
		}
		aux = aux->siguiente;
	}
	return 0;
}

//Devuelve un 1 si existen todas las dependencias de un string de dependencias
//0 en caso contrario
int existenDependencias (Tarea *lista, char *dep) {
	int *parse;
	int n;
	parse = analizarDependencias(dep, &n, ',');
	int i = 0;
	for(i = 0; i < n; i++){
		int dependencia = parse[i];
		if(!dependenciaEnLista(lista, dependencia)){
			return 0;
		}
	}
	return 1;
}


//Funcion que busca las Tareas dependientes directamente de la tarea
void buscarDependencias(Tarea *lista,int ids){
	char str[130];
	int i=0;
	char *token;
	int num;
	while(lista!=NULL){
		//recorro la lista para buscarlos
		if(!strncmp(lista->dependencias, "-", 1)){
			lista=lista->siguiente;
		}
		else{
			//strtok descompone el string ej:1,2,3 en los caracteres 1,2 y 3
			strcpy(str,lista->dependencias);
			token=strtok(str,",");
			while(token!=NULL){
				//paso esos caracteres a numeros y los comparo con el id de la tarea
				num=atoi(token);
				if(num==ids){
					if(i==0){
						printf("%d",lista->id);
						i++;
					}else{
						printf(",");
						printf("%d",lista->id);

					}
				}
				//paso al siguiente número
				token=strtok(NULL,",");
				
			}
			lista=lista->siguiente;
		}
	}	
	return;
}


//Funcion que imprime por pantalla las dep indirectas
void DependenciasIndir(Tarea *lista,int idTar, Set *s){
	//Recorro la lista hasta llegar al final
	
	while(lista!=NULL){
		//busco los identificadores que sean iguales a las dependencias directas(idTar)
		if(lista->id==idTar){
			//comparo con - para saber si tiene dependencias
			//si tiene:
			if(strncmp(lista->dependencias, "-", 1)){
				int *parse;
				int tam = 0;
				parse = analizarDependencias(lista->dependencias, &tam, ',');
				int i = 0;
				for(i = 0; i < tam; i++){
					pushSet(s, parse[i]);
				}
				break;
			}else{
				break;
			}
		//si el id de la tarea no coincide con la dependencia directa->sigo
		}else{
			lista=lista->siguiente;
		}
			
	}
		
	return;
}