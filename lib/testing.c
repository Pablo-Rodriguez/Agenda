
#include "../header/testing.h"

char *testFile = NULL;
char *testMode = NULL;
int testing = 0;

//Inicializa y configura el testing 
int configTest (char *fileName, char *mode) {
	if(strcmp(mode, "w") && strcmp(mode, "a")){
		return 0;
	}
	if(testFile != NULL){
		free(testFile);
		testFile = NULL;
	}
	testFile = (char*)malloc(strlen(fileName)*sizeof(char));
	strcpy(testFile, fileName);
	if(testMode != NULL){
		free(testMode);
		testMode = NULL;
	}
	testMode = (char*)malloc(strlen(mode)*sizeof(char));
	strcpy(testMode, mode);
	if(!strcmp(mode, "w")){
		FILE *pf;
		pf = fopen(testFile, "w");
		fprintf(pf, "Logging starting\n");
		fclose(pf);
		strcpy(testMode, "a");
	}
	return 1;
}

//Imprime por pantalla la configuracion del testing
void printConfig () {
	printf("\t%s\n", testFile);
	printf("\t%s\n", testMode);
	printf("\t%d\n", testing);
	return;
}

//Empieza el testing
void startTesting () {
	testing = 1;
	return;
}

//Para el testing
void stopTesting () {
	testing = 0;
	return;
}

//Loguea un string en el fichero de testing
int logger (char *text) {
	FILE *pf;
	pf = fopen(testFile, testMode);
	if(pf == NULL){
		return 0;
	}
	fprintf(pf, "%s", text);
	fclose(pf);
	return 1;
}