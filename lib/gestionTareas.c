
#include "../header/gestionTareas.h"

/*Funciones de operacion del programa.*/

Tarea *crearTarea (Tarea *lista, char *usuario) {
	system("clear");
	Tarea *t;
	t = malloc(sizeof(Tarea));
	printf("\n\tSe ha seleccionado crear una tarea.\n");
	printf("\tIntroduzca los datos de la nueva tarea.\n");

	//Tipo
	printf("\tIntroducir tipo de tarea: ");
	scanf(" %s", t->tipo);
	while(strcmp(t->tipo, "simple") && strcmp(t->tipo, "con_plazo")){
		printf("\t\033[31mNo ha introducido una opcion valida.\x1b[0m\n");
		printf("\tUso: [simple | con_plazo]\n");
		printf("\tIntroducir tipo de tarea: ");
		scanf(" %s", t->tipo);
	}

	//Plazo
	if(!strcmp(t->tipo, "con_plazo")){
		while(1){
			printf("\tIntroducir plazo: ");
			scanf(" %s", t->plazo);
			if(esUnaFecha(t->plazo)){
				if(esFechaFutura(t->plazo)){
					break;
				}else{
					printf("\tAVISO:\033[31m  La fecha debe ser como minimo 7 dias posterior a la actual.\x1b[0m\n");
				}
			}else{
				printf("\t\033[31mNo ha introducido una fecha correcta\x1b[0m\n");
				printf("\tFormato: dd/mm/aaaa:hh:mm\n");
			}
		}
	}else{
		strcpy(t->plazo, "-");
	}

	//Descripcion
	scanf("%*c");
	while(1){
		printf("\tIntroducir la descripcion de la tarea: ");
		fgets(t->descripcion, 50, stdin);
		limpiarBuffer(t->descripcion, 50);
		if(tareaExistente(lista, t->descripcion)){
			printf("\tAVISO:\033[31m  Tarea ya existente. Introduzca una descripcion distinta.\x1b[0m\n");
		}else{
			break;
		}
	}

	//Usuario
	strcpy(t->propietario, usuario);

	//Prioridad
	char c[3];
	while(1){
		printf("\tIntroducir prioridad: ");
		fgets(c, 3, stdin);
		c[2] = '\0';
		t->prioridad = atoi(c);
		if(t->prioridad < 1 || t->prioridad > 10){
			printf("\t\033[31mNo ha introducido una opcion valida.\x1b[0m\n");
			printf("\tIntroduzca un numero de 1 a 10.\n");
			if(t->prioridad != 0){
				limpiarBuffer(c, 2);
			}
		}else{
			break;
		}
	}
	if(t->prioridad == 10){
		getchar();
	}

	//Estado
	strcpy(t->estado, "pendiente");

	//Dependencias
	while(1){
		printf("\tIntroducir dependencias: ");
		fgets(t->dependencias, 50, stdin);
		if(!strncmp(t->dependencias, "0", 1)){
			strcpy(t->dependencias, "-");
			break;
		}else{
			if(sonDependencias(t->dependencias)){
				if(existenDependencias(lista, t->dependencias)){
					quitarSalto(t->dependencias, strlen(t->dependencias));
					break;
				}else{
					printf("\tAVISO:\033[31m Algunas de las dependencias indicadas es incorrecta\x1b[0m\n");
				}
			}else{
				printf("\t\033[31mNo ha introducido las dependencias de forma correcta\x1b[0m\n");
				printf("\tIntroduzca los identificadores separados por comas (,) o 0 si no quiere dependencias.\n");
			}
		}
	}
	lista = anhadirALista(t, lista);
	printf("\tPulse enter para continuar...");
	return lista;
}

void verTarea (Tarea *lista,char *usuario) {
	int ids,dias=0;
	Tarea *p_aux;	
	system("clear");
	if(lista==NULL){
		printf("\n\tAVISO: \033[31m lista de tareas vacia. \x1b[0m\n");
		printf("\n\tPulsa ENTER para volver al programa principal. ");
		borrarBuffer();
		return;
	}
	printf("\t\tIntroducir el identificador de la tarea: ");
	scanf(" %d",&ids);
	p_aux = lista; //p_aux puntero auxiliar al principio de la lista. 
	while(p_aux){
		if(p_aux->id==ids){
			break;
				/* El bucle while termina cuando se encuentra el id. Cuando se interrumpe el bucle el
				puntero p_aux apunta al nodo que contiene la tarea correspondiente. */ 
		}
	p_aux = p_aux->siguiente;
	}
	//Aviso de tarea no existente
	if(p_aux == NULL){ 
		printf("\n\tAVISO:\033[31m  Tarea no existente.\x1b[0m");
		printf("\n\n\tPulsar ENTER para volver al menu principal del programa: ");
		getchar();
		return; 
	}
	//Imprimo por pantalla las características de la tarea
	//La linea con la informacion de los dias que restan
	//para finalizar una tarea no debe aparecer para tareas 
	//de tipo simple ni para tareas con plazo pero ya terminadas.
	if(strcmp(p_aux->propietario,usuario)==0){
		system("clear");
		lineas(1, 80, '*');
		printf("\t\t\t\t");
		printf ("    TAREA %d",ids);
		printf("\t\t\t\t\n");
		lineas(1, 80, '*');
		printf("\n");
		printf("Descripcion: ");
		puts(p_aux->descripcion);
		printf("Prioridad: %d\n", p_aux->prioridad);
		printf("Estado: %s\n", p_aux->estado);
		printf("Tipo: %s\n", p_aux->tipo);
		//Si es de tipo con plazo imprimo el plazo y los días que faltan, si no no.
		if(!strcmp(p_aux->tipo,"con_plazo") && !strcmp(p_aux->estado,"pendiente")){
			printf("Plazo:" );
			puts(p_aux->plazo);
			printf("Dias para terminar la tarea: ");
			dias=diasQueFaltan(p_aux->plazo);
			printf("%d",dias);
			printf("\n");
		}
		//si no depende de nada, no hay dependencias directas ni indirectas
		printf("Tareas de las que depende la tarea: \n");
		if(!strncmp(p_aux->dependencias, "-", 1)){
			printf("\tDirectamente:- \n");
			printf("\tIndirectamente: -\n\n");

		}
		//si depende de algo:
		else{
			printf("\tDirectamente: ");
			char string[130];
			char *tok;
			int num,j=0;
			strcpy(string,p_aux->dependencias);
			tok=strtok(string,",");
			while(tok!=NULL){
				if(existenDependencias(lista,tok)==1){
					num=atoi(tok);
					if(j==0){
						printf("%d",num);
						j++;
					}else{
						printf(",");
						printf("%d",num);
					}

				}
				tok=strtok(NULL,",");
			}

			printf("\n\tIndirectamente: ");
			char s[130];
			char *t;
			int idTar;
			strcpy(s,p_aux->dependencias);
			t=strtok(s,",");
			Set *dep = malloc(sizeof(Set));
			initSet(dep);
			while(t!=NULL){
				idTar=atoi(t);
				DependenciasIndir(lista,idTar, dep);
				t=strtok(NULL,",");
			}
			printSet(dep);
			liberarSet(dep);
			dep = NULL;
				
		}
		
		printf("\n\n");

		//busco las tareas dependientes con la funcion buscarDependencias
		printf("Tareas dependientes directamente de la tarea: ");
		buscarDependencias(lista,ids);
		printf("\n");
		lineas(1, 80, '*');
		
	}


	else{
		printf("\n\tAVISO:\033[31m Operacion no permitida sobre esta tarea.\x1b[0m\n");
	
	}

	printf("\n");
	scanf("%*c");
	printf("\tPulsar ENTER para volver al menu principal del programa:");
	return;
}

int verLista (Tarea *lista, char *usuario) {
	char option1; //Opciones del submenu 1)
	int mod=0; 
	char eleccion='0';
	int hayTareas=0;
	Tarea *aux;
	aux = lista;

	int espera;
	//Submenu 1 (Seleccionar que tareas se incluyen en el listado a presentar)
	system("clear");
	while(mod == 0){
		printf("\t1) Pendientes\n");
		printf("\t2) Terminadas\n");
		printf("\t3) Todas\n");
		printf("\t0) Cancelar\n");
		printf("\tTareas a presentar?:");
		scanf(" %c",&option1);

		//Opciones del submenu 1 segun lo indicado (option1)
		switch(option1) {
			case '1':
				//Tareas pendientes
				mod=1;
				eleccion='1';
				//Comprueba que el usuario tiene tareas del tipo indicado en la opcion
				//De ser asi las suma al contador 
				while(aux != NULL){
					if(strcmp(aux->propietario,usuario)==0 && strcmp(aux->estado,"pendiente")==0){
						hayTareas++;
					}
					aux = aux->siguiente;
				}				
				if(hayTareas==0){
					printf("\n\tAVISO:\033[31m El usuario no tiene tareas pendientes para mostrar.\x1b[0m\n");
					espera = 1;
				}else{
					system("clear");
					//llama a la funcion presenta resultados, que recibe la lista, la opcion elegida
					//en el submenu 1 y el string con el nombre del usuario que ha entrado
					espera = presentaResultados(lista, eleccion, usuario);
				}
				if(espera == 1) {
					//espera a que el usuario pulse enter y sale de la funcion al menu principal
					scanf("%*c");
					printf("\n\tPulsar ENTER para volver al menu principal del programa: ");
					borrarBuffer();
				}
				break;

			case '2':
				//Tareas terminadas
				mod=1;
				eleccion='2';
				while(aux != NULL){
					if(strcmp(aux->propietario,usuario)==0 && strcmp(aux->estado,"terminada")==0){
						hayTareas++;
					}
					aux = aux->siguiente;
				}
				if(hayTareas==0){
					printf("\n\tAVISO:\033[31m El usuario no tiene tareas terminadas para mostrar.\x1b[0m\n");
					espera = 1;
				}else{
					system("clear");
					espera = presentaResultados(lista, eleccion, usuario);
					
				}
				if(espera == 1){
					scanf("%*c");
					printf("\n\tPulsar ENTER para volver al menu principal del programa: ");
					borrarBuffer();
				}
				break;

			case '3':
				//Todas las tareas
				mod=1;
				eleccion='3';
				while(aux != NULL){
					if(strcmp(aux->propietario,usuario)==0){
						hayTareas++;
					}
					aux = aux->siguiente;
				}
				if(hayTareas == 0){
					printf("\n\tAVISO:\033[31m El usuario no tiene tareas para mostrar.\x1b[0m\n");
					espera = 1;
				}else{
					system("clear");
					espera = presentaResultados(lista, eleccion, usuario);
				}
				if(espera == 1){
					scanf("%*c");
					printf("\n\tPulsar ENTER para continuar..");
					borrarBuffer();
				}
  				break;

			case '0': 
				//cuando se seleccione salir, limpia el buffer, limpia la pantalla y sale de la funcion
				borrarBuffer();
				system ("clear");
				return 0; //Sale de la funcion ver lista 

			default:
				//en caso de introducir una opcion erronea pone mensaje de error y vuelve a presentar
				//el menu
				system("clear");
				borrarBuffer();
				printf("\n\t\033[31mOpcion invalida. Seleccione 1,2,3 o 0 y pulse enter.\x1b[0m\n\n");
				break;
		}
	}
	return 0;
}

Tarea *eliminarTarea (Tarea *lista, char *usuario) {
	system("clear");
	//Variables auxiliares para la manipulacion de la lista
	Tarea *aux = lista, *aux2 = lista;

	//Pido el id de la tarea a borrar
	int id = 0;
	//String con el id para comprobar que son digitos
	char idS[20];
	while(1){
		printf("\tIntroduzca el identificador de la tarea que desea eliminar\n\t");
		scanf(" %s", idS);
		if(!strnum(idS, strlen(idS))){
			printf("\tERROR: \033[31mNo ha introducido un identificador adecuado.\x1b[0m\n");
		}else{
			break;
		}
	}
	id = atoi(idS);

	//Caso particular del primer elemento
	if(aux->id == id){
		if(!strcmp(aux->propietario, usuario)){
			lista = aux->siguiente;
			aux->siguiente = NULL;
			free(aux);
			aux = NULL;
			printf("\tSe ha eliminado la tarea con identificador %d.\n", id);
		}else{
			printf("\tAVISO: \033[31mNo se ha podido eliminar la tarea %d porque no le pertenece.\x1b[0m\n", id);
		}
		return lista;
	}

	//Caso normal
	//Recorro la lista en busca del id
	aux = aux->siguiente;
	while(aux){
		if(aux->id == id){
			if(!strcmp(aux->propietario, usuario)){
				aux2->siguiente = aux->siguiente;
				aux->siguiente = NULL;
				free(aux);
				aux = NULL;
				printf("\tSe ha eliminado la tarea con identificador %d.\n", id);
			}else{
				printf("\tAVISO: \033[31mNo se ha podido eliminar la tarea %d porque no le pertenece.\x1b[0m\n", id);
			}
			return lista;
		}
		aux = aux->siguiente;
		aux2 = aux2->siguiente;
	}

	if(aux == NULL){
		printf("\tERROR: \033[31mLa tarea con identificador %d no existe.\x1b[0m\n", id);
	}
	
	return lista;
}

void terminarTarea (Tarea *lista, char *usuario) {
	system("clear");
	//Pido el id de la tarea a borrar
	int id = 0;
	//String con el id para comprobar que son digitos
	char idS[20];
	while(1){
		printf("\tIntroduzca el identificador de la tarea que desea terminar\n\t");
		scanf(" %s", idS);
		if(!strnum(idS, strlen(idS))){
			printf("\tERROR: \033[31mNo ha introducido un identificador adecuado.\x1b[0m\n");
		}else{
			break;
		}
	}
	id = atoi(idS);

	while(lista != NULL){
		if(lista->id == id){
			if(strcmp(lista->propietario, usuario)){
				printf("\tLa tarea que pretende cambiar a terminada no le pertenece.\n");
			}else{
				printf("\tSe ha terminado la tarea con identificador %d.\n", id);
				strcpy(lista->estado, "terminada");
			}
			break;
		}
		lista = lista->siguiente;
	}

	if(lista == NULL){
		printf("\tAVISO: \033[31mLa tarea que quiere cambiar a terminada no existe.\x1b[0m\n");
	}
	return;
}

void salir (Tarea *lista, int mod) {
	char c;

	if(mod == 0){
		exit(1);
	}
	system("clear");
	while(1){
		printf("\tSe han modificado datos.\n");
		printf("\t1) Guardar cambios.\n");
		printf("\t2) Descartar cambios.\n");
		printf("\t0) Cancelar.\n");
		printf("\tOpcion?: ");
		scanf(" %c", &c);
		switch(c) {
			case '1':
				escribirTareasEnBD(lista);
				liberarLista(lista);
				lista = NULL;
				system("clear");
				exit(1);
				break;
			case '2':
				liberarLista(lista);
				lista = NULL;
				system("clear");
				exit(1);
				break;
			case '0':
				system("clear");
				return;
			default:
				printf("\tNo ha introducido una opcion valida.\n");
				system("clear");
				printf("\tELECCION INCORRECTA: PULSAR 1, 2 O 0.\n");
		}
	}
}

