
#include "../header/set.h"

//Inicializa una estructura de tipo Set
void initSet(Set *s){
	s->arr = NULL;
	s->tam = 0;
	return;
}

//Anhade un elemento a un set solo si este no estaba en el aun
void pushSet(Set *s, int elem){
    if(!estaEnElArray(elem, s->arr, s->tam)){
    	s->tam++;
    	if(s->arr == NULL){
    		s->arr = malloc(s->tam*sizeof(int));
    	}else{
    		s->arr = realloc(s->arr, s->tam*sizeof(int));
    	}
		s->arr[s->tam - 1] = elem;
    }
    return;
}

//Imprime el set por pantalla
//formato: n1,n2,n3...,nn
void printSet(Set *s){
	int i = 0;
	for(i = 0; i < s->tam; i++){
		printf("%d", s->arr[i]);
		if(i != s->tam - 1){
			printf(",");
		}
	}
	return;
}

//Libera la memoria reservada para el array del set
void liberarSet (Set *s) {
	free(s->arr);
	s->arr = NULL;
	s->tam = 0;
	return;
}