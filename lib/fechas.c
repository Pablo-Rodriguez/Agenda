
#include "../header/fechas.h"

//Devuelve 1 si el anho es bisiesto y 0 si no lo es
int esBisiesto (int anho) {
	if(anho % 4 == 0){
		return 1;
	}else{
		return 0;
	}
}

//Devuelve 1 si la fecha esta bien formateada y 0 en caso contrario
int esUnaFecha (char *fecha) {
	char diaS[3], mesS[3], anhoS[5], horaS[3], minsS[3];
	int dia, mes, anho, hora, mins;
	if(strlen(fecha) != 16){
		return 0;
	}
	if(fecha[2] != '/' || fecha[5] != '/' || fecha[10] != ':' || fecha[13] != ':'){
		return 0;
	}
	memcpypos(diaS, fecha, 2, 0);
	memcpypos(mesS, fecha, 2, 3);
	memcpypos(anhoS, fecha, 4, 6);
	memcpypos(horaS, fecha, 2, 11);
	memcpypos(minsS, fecha, 2, 14);
	if(!strnum(diaS, 2) || !strnum(mesS, 2) || !strnum(anhoS, 4) || !strnum(horaS, 2) || !strnum(minsS, 2)){
		return 0;
	}

	dia = atoi(diaS);
	mes = atoi(mesS);
	anho = atoi(anhoS);
	hora = atoi(horaS);
	mins = atoi(minsS);

	if(mes > 12 || mes < 1){
		return 0;
	}else{
		if((mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) && (dia > 31 || dia < 1)){
			return 0;
		}else if((mes == 4 || mes == 6 || mes == 9 || mes == 11) && (dia > 30 || dia < 1)){
			return 0;
		}else if(mes == 2){
			if(esBisiesto(anho) && (dia > 29 || dia < 1)){
				return 0;
			}else if(!esBisiesto(anho) && (dia > 28 || dia < 1)){
				return 0;
			}
		}
	}

	if(hora > 23 || mins > 59){
		return 0;
	}
	
	return 1;
}

//Devuelve en un array de 5 elementos dia mes anho hora y mins en enteros
void analizarFecha (char *fecha, int *destino) {
	char diaS[3], mesS[3], anhoS[5], horaS[3], minsS[3];
	memcpypos(diaS, fecha, 2, 0);
	memcpypos(mesS, fecha, 2, 3);
	memcpypos(anhoS, fecha, 4, 6);
	memcpypos(horaS, fecha, 2, 11);
	// //Problemas con basura en memoria
	// horaS[2] = '\0';
	memcpypos(minsS, fecha, 2, 14);

	destino[0] = atoi(diaS);
	destino[1] = atoi(mesS);
	destino[2] = atoi(anhoS);
	destino[3] = atoi(horaS);
	destino[4] = atoi(minsS);

	return;
}

//Devuelve un 1 si el mes es de 31 dias y 0 en caso contrario
int mes31 (int mes) {
	if(mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12){
		return 1;
	}else{
		return 0;
	}
}

//Devuelve un 1 si el mes es de 30 y un 0 en caso contrario
int mes30 (int mes) {
	if(!mes31(mes) && mes != 2){
		return 1;
	}else{
		return 0;
	}
}

//Devuelve un 1 si la fecha es como minimo 7 dias 
//posterior a la actual, 0 en caso contrario
int esFechaFutura (char *fecha) {
	int parse[5];
	time_t aux = time(0);
	struct tm *now = localtime(&aux);
	analizarFecha(fecha, parse);

	int difhour = parse[3] - now->tm_hour;
	int difmins = parse[4] - now->tm_min;
	
	now->tm_mday += 7;
	int diaActual = now->tm_mday;
	if(diaActual > 31 && mes31(now->tm_mon)){
		diaActual -= 31;
		now->tm_mon++;
	}else if(diaActual > 30 && mes30(now->tm_mon)){
		diaActual -= 30;
		now->tm_mon++;
	}else if(diaActual > 29 && now->tm_mon + 1 == 2 && esBisiesto(now->tm_year + 1900)){
		diaActual -= 29;
		now->tm_mon++;
	}else if(diaActual > 28 && now->tm_mon + 1 == 2 && !esBisiesto(now->tm_year + 1900)){
		diaActual -= 28;
		now->tm_mon++;
	}

	if(now->tm_mon + 1 > 12){
		now->tm_mon -= 12;
		now->tm_year++;
	}

	int difyear = parse[2] - (now->tm_year + 1900);
	int difmonth = parse[1] - (now->tm_mon + 1);
	int difday = parse[0] - (now->tm_mday);


	if(difyear == 0){
		if(difmonth == 0){
			if(difday == 0){
				if(difhour == 0){
					if(difmins == 0){
							return 0;
					}else if(difmins < 0){
						return 0;
					}else{
						return 1;
					}
				}else if(difhour < 0){
					return 0;
				}else{
					return 1;
				}
			}else if(difday < 0){
				return 0;
			}else{
				return 1;
			}
		}else if(difmonth < 0){
			return 0;
		}else{
			return 1;
		}
	}else if(difyear < 0){
		return 0;
	}else{
		return 1;
	}
	
	return 1;
}

//Devuelve la fecha actual en forma de estructura tm
struct tm fechaActual(){

	struct tm *fecha_hora; // tm es un tipo estructurado definido en <time.h> 
                                   //que contiene cada campo de fecha y hora
	time_t segundos; 
 
    segundos = time(NULL); // obtiene los segundos desde 1-1-1970 
    fecha_hora=localtime(&segundos); // convierte los 'segundos' en la hora local 

    fecha_hora->tm_mon+=1;
    fecha_hora->tm_year+=1900;

    return *fecha_hora;
}

//Devuelve el numero de dias que quedan hasta la fecha introducida
//formato: dd/mm/aaaa:hh:mm
int diasQueFaltan(char *fechaTarea){
	int diasQueFaltan=0;
	char *p=NULL;
	p=fechaTarea;
	struct tm fecha1;
	struct tm fecha2;
	//22/11/2013:22:13 VOY AVANZANDO EL PUNTERO
	fecha1.tm_mday=atoi(p);
	p=p+3;
	fecha1.tm_mon=atoi(p);
	p=p+3;
	fecha1.tm_year=atoi(p);
	p=p+5;
	fecha1.tm_hour=atoi(p);
	p=p+3;
	fecha1.tm_min=atoi(p);
	fecha1.tm_sec=0;
	fecha2 = fechaActual();
	diasQueFaltan=difftime(mktime(&fecha1),mktime(&fecha2));


	return diasQueFaltan/60/60/24;
}