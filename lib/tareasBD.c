
#include "../header/tareasBD.h"

//Carga todas las tareas de la base de datos en una lista encadenada
//y devuelve un puntero al primer elemento de dicha lista
Tarea *cargarTareas () {
	//Definimos una variable de tipo *Tarea que apunta a la primera de la lista y otra auxiliar
	Tarea *lista = NULL, *aux;
	//Definimos el fichero de la base de datos
	FILE *bd; //bd por base de datos
	//Creamos una variable auxiliar para el tipo de cada elemento
	char tipo[15];
	//Abrimos el fichero
	bd = fopen(BASE_DE_DATOS, "r");

	if(bd == NULL){
		printf("\tERROR: no se puede abrir el fichero %s\n", BASE_DE_DATOS);
		exit(1);
	}

	while(fgets(tipo, 15, bd) != NULL){
		quitarSalto(tipo, 15);
		if(lista == NULL){
			lista = malloc(sizeof(Tarea));
			strcpy(lista->tipo, tipo);
			fscanf(bd, "%d", &(lista->id));
			fgetc(bd);
			fgets(lista->descripcion, 50, bd);
			quitarSalto(lista->descripcion, strlen(lista->descripcion));
			fscanf(bd, " %s", lista->propietario);
			fgetc(bd);
			fscanf(bd, "%d", &(lista->prioridad));
			fgetc(bd);
			fscanf(bd, " %s", lista->estado);
			fgetc(bd);
			fscanf(bd, " %s", lista->plazo);
			fgetc(bd);
			fscanf(bd, " %s", lista->dependencias);
			fgetc(bd);
			//Me libro del * que separa tareas metiendolo en tipo
			fgets(tipo, 10, bd);
			lista->siguiente = NULL;
			aux = lista;
		}else{
			aux->siguiente = malloc(sizeof(Tarea));
			aux = aux->siguiente;
			strcpy(aux->tipo, tipo);
			fscanf(bd, "%d", &(aux->id));
			fgetc(bd);
			fgets(aux->descripcion, 50, bd);
			quitarSalto(aux->descripcion, strlen(aux->descripcion));
			fscanf(bd, " %s", aux->propietario);
			fgetc(bd);
			fscanf(bd, "%d", &(aux->prioridad));
			fgetc(bd);
			fscanf(bd, " %s", aux->estado);
			fgetc(bd);
			fscanf(bd, " %s", aux->plazo);
			fgetc(bd);
			fscanf(bd, " %s", aux->dependencias);
			fgetc(bd);
			//Me libro del * que separa tareas metiendolo en tipo
			fgets(tipo, 10, bd);
			aux->siguiente = NULL;
		}
	}

	fclose(bd);

	return lista;
}

//Funcion que dada una lista de tareas te devuelve el numero de tareas
int tamanhoLista (Tarea *lista) {
	//No hace falta crear una variable auxiliar porque solo queremos leer
	//Y no va a afectar al resto del programa

	int n = 0;

	if(lista == NULL){
		return 0;
	}

	while(lista->siguiente != NULL){
		lista = lista->siguiente;
		n++;
	}

	return ++n;
}

//Libera la memoria de una lista de tareas que ya no sea necesaria
void liberarLista (Tarea *lista) {
	//Declaro un puntero a tarea como variable auxiliar
	Tarea *aux = lista;
	if(aux == NULL){
		return;
	}

	while(aux->siguiente != NULL){
		aux = lista->siguiente;
		lista->siguiente = NULL;
		free(lista);
		lista = aux;
	}
	free(aux);
	aux = NULL;
	lista = NULL;
	return;
}

//Devuelve un 1 si existe una tarea con descripcion = al arg 2 en lista. 0 en caso contrario
int tareaExistente (Tarea *lista, char *descripcion) {
	char aux[50];
	strcpy(aux, descripcion); 
	quitarSalto(aux, (int)strlen(aux));
	while(lista != NULL){
		if(!strcmp(lista->descripcion, aux)){
			return 1;
		}
		lista = lista->siguiente;
	}
	return 0;
}

//Devuelve el id mas bajo que este disponible
int buscarIdDisponible (Tarea *lista) {
	//declaro una variable entera que contenga el numero de tareas que se cargaron
	int n = 0;
	//Array de id's
	int *ids;
	//Cargo las tareas y asigno la direccion de la primera a lista
	n = tamanhoLista(lista);
	if(lista == NULL || n == 0){
		return 1;
	}
	//Reservo memoria para n enteros que seran los distintos id's de las tareas
	ids = malloc(n*sizeof(int));

	//Recorro la lista y introduzco los id's en el array
	int i = 0;
	for(i = 0; i < n; i++){
		*(ids + i) = lista->id;
		lista = lista->siguiente;
	}

	//Busco el numero mas bajo disponible
	i = 1;
	while(estaEnElArray(i, ids, n)){
		i++;
	}
	return i;
}

//Escribe en la base de datos una tarea nueva
void escribirTareasEnBD (Tarea *lista) {
	//Declaro un puntero a archivo para escribir en la bd
	FILE *bd;
	//Variable auxiliar para recorrer la lista
	Tarea *aux;
	aux = lista;
	//Abro el archivo en modo append para escribir al final
	while(1){
		bd = fopen(BASE_DE_DATOS, "w");

		if(bd == NULL){
			printf("\tERROR: No se ha podido guardar en %s\n", BASE_DE_DATOS);
			printf("\tPuede perder los datos de esta sesion.\n");
			scanf("%*c");
			printf("\tEste problema suele darse por falta de permisos de escritura sobre el fichero\n");
			printf("\tCorrija el problema y pulse enter para continuar\n");
			fgetc(stdin);
		}else{
			break;
		}
	}

	//Escribo todos os datos
	while(aux != NULL){
		fprintf(bd, "%s\n", aux->tipo);
		fprintf(bd, "%d\n", aux->id);
		fprintf(bd, "%s\n", aux->descripcion);
		fprintf(bd, "%s\n", aux->propietario);
		fprintf(bd, "%d\n", aux->prioridad);
		fprintf(bd, "%s\n", aux->estado);
		fprintf(bd, "%s\n", aux->plazo);
		fprintf(bd, "%s\n", aux->dependencias);
		if(aux->siguiente == NULL){
			fprintf(bd, "*");
		}else{
			fprintf(bd, "*\n");
		}
		aux = aux->siguiente;
	}
	fclose(bd);

	return;
}

//Anhade una tarea a la lista de tareas
Tarea *anhadirALista (Tarea *t, Tarea *lista) {
	//Declaramos un puntero a tareas como variable
	//auxiliar para recorrer la lista
	Tarea *aux;
	aux = lista;

	//Anhado a la tarea los campos que faltan
	t->id = buscarIdDisponible(lista);
	//Corrijo el \n que tiene la descripcion
	quitarSalto(t->descripcion, 49);
	t->descripcion[49] = '\0';
	if(lista == NULL){
		lista = t;
	}else{
		//Avanzamos a la ultima tarea
		while(aux->siguiente != NULL){
			aux = aux->siguiente;
		}

		//Enlazamos la tarea nueva al final de la lista
		aux->siguiente = t;
	}

	return lista;
}