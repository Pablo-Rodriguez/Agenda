
#include "../header/util.h"

//Copia tam caracteres de org a dest desde tam[pos]
void memcpypos (char *dest, char *org, int tam, int pos) {
	int posInit = pos;
	for(pos = 0; pos < tam; pos++){
		dest[pos] = org[pos + posInit];
	}
	return;
}

//Devuelve 1 si el string solo tiene caracteres numericos 
//en las n primeras posiciones y 0 en caso contrario
int strnum (char *str, int n) {
	int i = 0;
	if(n > strlen(str)){
		n = strlen(str);
	}
	for(i = 0; i < n; i++){
		if(!isdigit(str[i])){
			return 0;
		}
	}
	return 1;
}

//Funcion que comprueba si un entero esta en un array de enteros.
//Devuelve 1 si esta y 0 si no.
int estaEnElArray (int n, int *arr, int tam) {
	int i = 0;
	for(i = 0; i < tam; i++){
		if(n == arr[i]){
			return 1;
		}
	}
	return 0;
}

//Funcion que cambia los \n por \0 en el una cantidad de caracteres de inicio (tam)
//tambien anhade un \0 al final
void quitarSalto (char *str, int tam) {
	int i = 0;
	if(tam > strlen(str)){
		tam = strlen(str);
	}
	for(i = 0; i < tam; i++){
		if(str[i] == '\n'){
			str[i] = '\0';
		}
		if((i+1) == tam){
			str[i] = '\0';
		}
	}
	return;
}

//limpia el buffer de entrada si se pasa el usuario al meter caracteres
void limpiarBuffer (char *str, int max) {
	if(strlen(str) >= max){
		while(getchar() != '\n');
	}
	return;
}

//Borra todo el buffer
void borrarBuffer () {
	while(getchar() != '\n');
	return;
}
