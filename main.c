
//librerías de C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//librerías propias de nuestro programa
//La libreria testing es solo para debuggin
#include "config.h"
#ifndef DESARROLLO
	#include "header/testing.h"
#endif
#include "header/util.h"
#include "header/set.h"
#include "header/tareasBD.h"
#include "header/pantalla.h"
#include "header/fechas.h"
#include "header/gestionDependencias.h"
#include "header/presentacionResultados.h"

int main (){
 	//Las dos lineas comentadas son para inicializar el testing
	// configTest("testLog.txt", "w");
	// startTesting();
	//Identificador del usuario que entra en la applicacion
	char usuario[21];
	//Lista de tareas
	Tarea *lista;
	lista = cargarTareas();
	//Variable que indica si hubo una modificacion de datos
	int mod = 0;
	
	//Carátula de la Agenda
	system("clear");
	printf("\n");
	lineas(2, 80, '*');
	titulo (" GESTOR DE TAREAS DE UNA AGENDA ", 80, '*');
	lineas(2, 80, '*');
	printf("\n");

	//Identificador de usuario

	printf("\tIntroduzca su identificador (max. 20 caracteres y sin espacios): \n");
	printf("\t\t\t\t");
	scanf(" %s", usuario);
	usuario[19] = '\0';
	borrarBuffer();
	system("clear");
	
	while(1){
		mostrarMenu();
		if(seleccionarOpcion(&lista, usuario, mod)){
			mod = 1;
		}
	}

  return 0;
}